---
layout: post
title: Must have cool online services
date: 2018-11-08 20:49 +0800
categories: [service]
tags: [services, analyzer]
---

Cool online services

Services                       | Description
------------------------------ | -----------
[fastthread][fastthread]       | Java Thread Dump Analyzer
[gceasy][gceasy]               | Universal GC Log Analyzer
[heaphero][heaphero]           | Java & Android Heap Dump Analyzer
[nimbleapp]                    | Functional Performance Testing for Android & iOS
[Newrelic Mobile][newrelic]    | Deliver great apps with mobile performance monitoring


[fastthread]: http://fastthread.io
[gceasy]: http://gceasy.io
[heaphero]: http://heaphero.io/
[nimbleapp]: https://nimble.app
[newrelic]: https://newrelic.com/mobile-monitoring
