---
layout: post
title: open source perf tools
date: 2018-09-26 21:23 +0800
categories: [Android]
tags: [perf, tools]
---
Tools                          | Description
------------------------------ | -----------
[avocado][avocado]             | Avocado is a next generation testing framework inspired by Autotest
[android-extras][extras]       | app-launcher, cppreopts, cpustats, crypto-perf, ioblame, ioshark, iotop, latencytop, memcpy-perf, memory_replay, memtrack, micro_bench, mmap-perf, pagecache, perfprofd, sane_schedstat, showmap, showslab, simpleperf, slideshow, systrace_analysis, taskstats, zram-perf
[android-perf-testing][apt]    | Automated Performance Testing on Android
[bcc][bcc]                     | BCC - Tools for BPF-based Linux IO analysis, networking, monitoring, and more
[bootchart][bc]                | An efficient and feature-rich system boot graphing tool implementation in pure C.
[bootchart][bootchart]         | merge of bootchart-collector and pybootchartgui
[catapult][catapult]           | Catapult
[dstat][dstat]                 | Versatile resource statistics tool
[fio][fio]                     | Flexible I/O Tester
[flamegraph][flamegraph]       | Stack trace visualizer
[glmark2][glmark2]             | glmark2 is an OpenGL 2.0 and ES 2.0 benchmark
[htop][htop]                   | htop is an interactive text-mode process viewer for Unix systems
[HeatMap][HeatMap]             | Heat map generation tools
[iperf][iperf]                 | iperf3: A TCP, UDP, and SCTP network bandwidth measurement tool
[latencytop][latencytop]       | Pretty old
[lkp-tests][lkp]               | Linux Kernel Performance tests
[numatop][numatop]             | NumaTOP is an observation tool for runtime memory locality characterization and analysis of processes and threads running on a NUMA system.
[openstf][openstf]             | Control and manage real Smartphone devices from your browser
[oprofile][oprofile]           | OProfile is an open source project that includes a statistical profiler for Linux systems, capable of profiling all running code at low overhead.
[perfetto][perfetto]           | Perfetto is an open-source project for performance instrumentation and tracing of Linux/Android/Chrome platforms and user-space apps.
[perf-tools]                   | Performance analysis tools based on Linux perf_events (aka perf) and ftrace
[phoronix-test-suite][pts]     | The Phoronix Test Suite open-source, cross-platform automated testing/benchmarking software.
[rt-app][rt-app]               | rt-app emulates typical mobile and real-time systems use cases and gives runtime information
[sched-profile][sched-profile] | A set of scripts and tools useful to profile the Linux scheduler
[schedtool-dl][schedtool-dl]   | A tool to change or query all CPU-scheduling policies under Linux
[speedscope][speedscope]       | A fast, interactive web-based viewer for performance profiles.
[stf][stf]                     | Control and manage Android devices from your browser.
[tiobench][tiobench]           | Threaded IO Benchmark
[trace-cmd][trace-cmd]         | trace-cmd and KernelShark
[workload-automation][wa]      | A framework for automating workload execution and measurement collection on ARM devices.

[avocado]: http://avocado-framework.github.io
[extras]: https://android.googlesource.com/platform/system/extras
[apt]: https://github.com/googlecodelabs/android-perf-testing
[bcc]: https://github.com/iovisor/bcc
[bc]: https://github.com/ahkok/bootchart
[bootchart]: https://github.com/xrmx/bootchart
[catapult]: https://github.com/catapult-project/catapult
[dstat]: https://github.com/dagwieers/dstat
[fio]: https://github.com/axboe/fio
[FlameGraph]: https://github.com/brendangregg/FlameGraph
[glmark2]: https://github.com/glmark2/glmark2
[htop]: https://github.com/hishamhm/htop
[HeatMap]: https://github.com/brendangregg/HeatMap
[iperf]: https://github.com/esnet/iperf
[latencytop]: http://git.infradead.org/latencytop.git
[lkp]: https://github.com/intel/lkp-tests
[numatop]: https://github.com/intel/numatop
[openstf]: https://openstf.io
[oprofile]: http://oprofile.sourceforge.net/download/
[perfetto]: https://github.com/catapult-project/perfetto
[perf-tools]: https://github.com/brendangregg/perf-tools
[pts]: https://github.com/phoronix-test-suite/phoronix-test-suite
[rt-app]: https://github.com/scheduler-tools/rt-app
[sched-profile]: https://github.com/derkling/sched-profile
[schedtool-dl]: https://github.com/scheduler-tools/schedtool-dl
[speedscope]: https://github.com/jlfwong/speedscope
[stf]: https://github.com/openstf/stf
[tiobench]: https://github.com/mkuoppal/tiobench
[trace-cmd]: https://git.kernel.org/pub/scm/linux/kernel/git/rostedt/trace-cmd.git
[wa]: https://github.com/ARM-software/workload-automation
